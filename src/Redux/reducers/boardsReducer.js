import { FETCH_BOARDS_REQUEST, FETCH_BOARDS_SUCCESS, FETCH_BOARDS_FAIL, POST_BOARD} from "../actions/actionTypes.js"


const initialState ={
    boards:[],
    error:"",
    isLoading:true
}

const boardReducers = (state=initialState,action) =>{
    switch (action.type) {
        case FETCH_BOARDS_REQUEST:
            return {...state,isLoading:true}
        case FETCH_BOARDS_SUCCESS:
            return {isLoading:false,boards:action.data,error:""}
        case FETCH_BOARDS_FAIL:
            return {isLoading:false,boards:[],error:action.data}
        case POST_BOARD:
            const boards = state.boards.concat(action.payload.data)
            return {...state,boards}
        default:
           return state
    }
}

export default boardReducers