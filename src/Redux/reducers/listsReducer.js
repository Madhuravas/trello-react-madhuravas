import { DELETE_LIST, FETCH_LISTS_DATA, POST_LIST } from "../actions/actionTypes"


const initialState = {
    lists:[]
}

const listsReduces = (state=initialState,action) =>{
    switch(action.type){
        case FETCH_LISTS_DATA:
            return {...state, lists:action.payload}
        case POST_LIST:
            const lists = state.lists.concat(action.payload)
            return {...state,lists}
        case DELETE_LIST:
            const newList = state.lists.filter(item =>{
                return item.id !== action.payload
            })
            return {...state,lists:newList}
        default:
            return state
    }
} 

export default listsReduces