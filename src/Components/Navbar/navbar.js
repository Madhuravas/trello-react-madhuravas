import { Component } from "react";
import MenuIcon from '@mui/icons-material/Menu';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import AddIcon from '@mui/icons-material/Add';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import SearchIcon from '@mui/icons-material/Search';

import "./navbar.css"


class Navbar extends Component {
    render() {
        return (
            <nav className="navbar-main-card">
                <div className="nav-card-one">
                    <MenuIcon style={{ color: "#ffffff" }} />
                    <p className="icon">Trello</p>
                    <div className="options">
                        <p className="options-text">Workspaces  </p>
                        <KeyboardArrowDownIcon style={{ color: "#ffffff" }} />
                    </div>
                    <div className="options">
                        <p className="options-text">Recent  </p>
                        <KeyboardArrowDownIcon style={{ color: "#ffffff" }} />
                    </div>
                    <div className="options">
                        <p className="options-text">More</p>
                        <KeyboardArrowDownIcon style={{ color: "#ffffff" }} />
                    </div>
                    <div className="options2">
                        <p className="options-text">More</p>
                        <KeyboardArrowDownIcon style={{ color: "#ffffff" }} />
                    </div>
                    < AddIcon style={{ color: "#ffffff", marginLeft: "12px" }} />
                </div >
                <div className="nav-card-one">
                    <input type="search" className="search-input" placeholder="search" />
                    <SearchIcon className="search-input2" />
                    <ErrorOutlineIcon className="error-outline-icon" />
                    <NotificationsNoneIcon style={{ color: "#ffffff", marginLeft: "5px" }} />
                    <div className="user-profile">
                        <p style={{ fontSize: ".9rem", color: "#ffffff", fontWeight: "600" }}>M</p>
                    </div>
                </div>
            </nav >
        )
    }
}

export default Navbar