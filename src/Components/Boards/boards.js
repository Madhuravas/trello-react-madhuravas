import { Component } from "react";
import { connect } from "react-redux";
import { Bars } from "react-loader-spinner";
import { Link } from "react-router-dom";

import CloseIcon from '@mui/icons-material/Close';

import { fetchBorders, postBoard } from "../../Redux/actions"

import "./boards.css"


class Boards extends Component {
    state = { showBoardInput: false, newBoardInput:"" }

    handleShowBorder = () => {
        this.setState({ showBoardInput: true })
    }

    closeBorderInputCard = () =>{
        this.setState({showBoardInput:false})
    }

    handleBoardInput = (event) =>{
       this.setState({newBoardInput:event.target.value})
    }

    handleCreateBoard = () =>{
        const {newBoardInput} = this.state
        this.props.postBoard(newBoardInput)
        console.log(newBoardInput)
        this.setState({newBoardInput:""})
    }



    showBorderInputCard = () => {
        const { showBoardInput,newBoardInput } = this.state
        if (showBoardInput) {
            return (
                <div className="board-popup-input-main-card">
                    <div className="board-input-popup-card">
                        <div className="board-input-card">
                            <h5 className="add-board-head">Add new Board</h5>
                            <button onClick={this.closeBorderInputCard} className="board-input-close"><CloseIcon /></button>
                        </div>
                        <div className="input-card">
                            <label>Board title*</label>
                            <input value={newBoardInput} onChange={this.handleBoardInput} className="input-enter-card" type="text" placeholder="Enter a board name"/>
                            <p>👋 Board title is required</p>
                            <button onClick={this.handleCreateBoard} className="board-create-btn">Create</button>
                        </div>
                    </div>
                </div>)
        }
    }

    componentDidMount() {
        this.props.fetchBorders()
    }

    render() {
        const { boards } = this.props
        let arr

        if (boards.boards !== [] && boards.boards !== undefined) {
            arr = this.props.boards.boards.reduce((acc, item) => {
                if (item.closed === false) {
                    acc.push(item)
                }
                return acc
            }, [])
        }


        if (boards.isLoading) {
            return (<div className="loader-container">
                <Bars type="ThreeDots" color="#0b69ff" height="50" width="50" />
            </div>)
        } else {
            if (arr !== undefined) {
                return (
                    <div className="main">
                    <div className="board-main-container">
                        {arr.map(item =>
                         <Link to={`/board-Id/${item.id}`} style={{color: 'inherit', textDecoration: 'inherit'}} key={item.id}>
                            <div className="each-board-container" >
                                <h4 className="board-name">{item.name}</h4>
                            </div>
                            </Link>
                        )}
                        <button onClick={this.handleShowBorder} className="add-board-button">Add new Card</button>
                        {this.showBorderInputCard()}
                    </div>
                    </div>
                )
            }
        }
    }
}


const mapStateToProps = (state) => {
    return {
        boards: state.boards
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchBorders: () => dispatch(fetchBorders()),
        postBoard: (newBoardInput) => dispatch(postBoard(newBoardInput))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Boards)