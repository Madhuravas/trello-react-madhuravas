import React from 'react';

import { BrowserRouter, Switch, Route ,Redirect} from "react-router-dom";

import Navbar from './Components/Navbar/navbar';
import Boards from './Components/Boards/boards';
import Lists from './Components/Lists/list';

import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" render={() =>{
          return (<Redirect to="/boards" />)
        }} />
        <Route exact path="/boards" component={Boards} />
        <Route exact path="/board-Id/:boardId" component={Lists}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
